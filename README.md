# Linkedin_posts

Projet N°2 - Deep Learning : Generateur de posts linkedin sur des articles de Data Science

Auteurs: Marieme Gueye, Rayann Hamrouni

# Architecture 
```
Linkedin_Posts
├── data
|   ├── .gitkeep
|   ├── README.md
├── generate_linkedin_post_from_blog_article
|    ├── Application
|    ├── Domain
|    ├── infrastructure
|    ├── interface
|    └── tests
├── models
|   ├── .gitkeep
|   ├── README.md
├── notebooks
|   ├── .gitkeep
├── scraping
|   ├── notebook
|   |   ├── .gitkeep
|   |   ├── extract_article.ipynb
|   |   └── extract_post_url.ipynb
|   ├── .gitkeep
|   ├── chromedriver
|   ├── get_urls_post.py
|   └── login_script.py
├── .gitignore
├── README.md
├── README.rst
├── __init__.py
├── activate.sh
├── init.sh
├── pyproject.toml

```
# How to use the app

## 1. Clone the repository
```
$ git clone https://gitlab.com/yotta-academy/mle-bootcamp/projects/dl-projects/fall-2020/linkedin_posts.git

$ cd linkedin post
```

## 2. Set the environnement

When you clone the project, run `init.sh` to install dependencies and create a new venv add the repository in the PYTHONPATH 
whenever you want to use this project locally, run activate.sh to activate the venv and add the repository in the PYTHONPATH
Run every python file with `poetry run`

## 3. Data
Add your training data(csv ou parquet file) in the folder data.
You must update the name of these file in: generate_linkedin_post_from_blog_article/config/config.py

## 4. Scrape data

To scrape linkedin post and get associated urls article, see `get_urls_post.py` from scraping directory

To scrape articles, see `extract_article.ipynb` from notebook directory in `scraping`

## 5. Train your model

If you want to train a model, use files from `generate_linkedin_post_from_blog_article/application/main/main_train` directory.

Models we fine tune for summarization are `t5`and `Mbart`.

## 6. Run the application

To run the application you just have to make :

``` 
poetry run streamlit run <path>/text_summarizer.py  for pretrained model 
poetry run streamlit run <path>/app.py  for finetune model( you have to put the model in the folder models, you must update the name of the model in generate_linkedin_post_from_blog_article/config/config.py ) 

```


