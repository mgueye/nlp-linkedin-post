import torch
from torch.utils.data import Dataset
from typing import Dict, Any, List
from generate_linkedin_post_from_blog_article.infrastructure.dataclass_linkedin import DataclassLinkedin

class DatasetLinkedin(Dataset):
    def __init__(self, dataclass_linkedins: List[DataclassLinkedin] ):
      self.dataclass_linkedins = dataclass_linkedins

    def __len__(self):
      """[summary]

      Returns:
          [int]: [len of the dataset]
      """
      return len(self.dataclass_linkedins)

    def __getitem__(self, index) -> Dict[str, Any]:
      dataclass_linkedin: DataclassLinkedin = self.dataclass_linkedins[index]
      return dataclass_linkedin