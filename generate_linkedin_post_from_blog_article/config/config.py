import os 

REPO_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../'))
DATA_DIR = os.path.join(REPO_DIR,'data')

DATA_FILE_NAME_TRAIN_VAL= 'dataset_train_val.csv'
FULL_DATA_DIR_TRAIN_VAL = os.path.join(DATA_DIR, DATA_FILE_NAME_TRAIN_VAL)

DATA_FILE_NAME_TEST= 'dataset_test.csv'
FULL_DATA_DIR_TEST = os.path.join(DATA_DIR, DATA_FILE_NAME_TEST)

DATA_FILE_NAME = 'dataset.csv'
FULL_DATA_DIR = os.path.join(DATA_DIR, DATA_FILE_NAME)

MODELS_DIR = os.path.join(REPO_DIR,'models')
MODEL_DIR_T5  = os.path.join(MODELS_DIR,'model_t5')
MODEL_DIR_MBART  = os.path.join(MODELS_DIR,'model_mbart')

MODEL_NAME = "t5" # or "mbart"
FULL_MODEL_DIR = os.path.join(MODELS_DIR,"model_" + MODEL_NAME)
LOG_DIR = os.path.join(REPO_DIR, 'logs')

MODEL_NAME_T5 = "t5"
MODEL_NAME_MBART = "mbart"

ARTICLE = "article"
POST = "linkedin_post"