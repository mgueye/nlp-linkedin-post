from generate_linkedin_post_from_blog_article.domain.models import Models
from generate_linkedin_post_from_blog_article.domain.train_model import TrainModel
from generate_linkedin_post_from_blog_article.infrastructure.load_data import LoadData
from generate_linkedin_post_from_blog_article.domain.batch_loader import BatchLoader
from generate_linkedin_post_from_blog_article.infrastructure.dataclass_linkedin import DataclassLinkedin
from generate_linkedin_post_from_blog_article.infrastructure.dataset_linkedin import DatasetLinkedin
import generate_linkedin_post_from_blog_article.config.config as cf
from transformers import AutoModelWithLMHead, AutoTokenizer
from transformers import MBartForConditionalGeneration, MBartTokenizer
from typing import Dict, Any, List



def summarizer(input_text, name_model):
    """[summary]

    Args:
        input_text ([string]): text to resumes
        name_model ([string])

    Returns:
        [string]: [summary]
    """
    if name_model=='t5':
        model_t5 = AutoModelWithLMHead.from_pretrained(cf.MODEL_DIR_T5)
        tokenizer_t5= AutoTokenizer.from_pretrained('t5-base')
        inputs = tokenizer_t5.encode("summarize: " + input_text, return_tensors="pt", max_length=512)
        outputs = model_t5.generate(inputs, max_length=128, min_length=128, length_penalty=2.0, num_beams=4, early_stopping=True, truncation=True)
        result = tokenizer_t5.decode(outputs[0])
        return result
    elif name_model=='mbart':
        tokenizer_mbart= MBartTokenizer.from_pretrained("facebook/mbart-large-cc25")
        model_bart = MBartForConditionalGeneration.from_pretrained(cf.MODEL_DIR_MBART)
        encoded = tokenizer_mbart.prepare_seq2seq_batch(src_texts=input_text, return_tensors="pt").data
        input_ids = encoded['input_ids'].to("cuda")
        attention_mask = encoded['attention_mask'].to("cuda")
        translated_tokens = model_bart.generate(input_ids=input_ids, attention_mask=attention_mask, decoder_start_token_id=tokenizer_mbart.lang_code_to_id["en_XX"])
        result = tokenizer_mbart.batch_decode(translated_tokens, skip_special_tokens=True)[0]
        return result
    else:
        model = Models(cf.MODEL_NAME)
        no_trained_model, tokenizer = model.get_model()
        space = " "
        inputs = tokenizer.encode("Get Post: " + input_text, return_tensors="pt", max_length=512)
        outputs = no_trained_model.generate(inputs, max_length=128, min_length=128, length_penalty=2.0, num_beams=4, early_stopping=True)
        result = tokenizer.decode(outputs[0])
        return result



def main_train(data_train_path):
    """[summary]

    Args:
        data_train_path ([String]): [the path to the csv file ]
    """
    data = LoadData(data_train_path)
    list_article, list_post, _= data.list_article_list_post()
    dataclass_linkedins : List[DataclassLinkedin] = [DataclassLinkedin(article=article, post=post_linkedin) for article, post_linkedin in zip(list_article, list_post)]
    train_size = int(len(dataclass_linkedins)*0.8)
    train_dataset = DatasetLinkedin(dataclass_linkedins = dataclass_linkedins[:train_size])
    val_dataset = DatasetLinkedin(dataclass_linkedins = dataclass_linkedins[train_size:])
    model = Models(cf.MODEL_NAME)
    model, tokenizer = model.get_model()
    data_kwargs = {
    'src_lang':'en_XX',
    'tgt_lang': 'en_XX'
    }
    batch = BatchLoader(tokenizer, data_kwargs)
    train = TrainModel(batch, train_dataset,val_dataset, model, tokenizer)
    train.train_model()
    
if __name__ == '__main__':
    print('Choose action :')
    print('1 : train new model')
    print('2 : summarize my text')
    choice = input('Your choice (defaults to 2) : ')
    if choice == '' : choice = '2'

    if choice == '1' :
        main_train(cf.DATA_FILE_NAME_TRAIN_VAL)
    if choice == '2' :
        print('Choose your model :')
        print('1 : t5')
        print('2 : mbart')
        model = input('Your choice (defaults to 1) : ')
        input_text = input('Put your text ')
        if model == '1':
            summarizer = summarizer(input_text, 't5')
        elif model == '2':
            summarizer = summarizer(input_text, 'mbart')
            
            
        


    

